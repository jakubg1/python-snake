import curses
from curses import wrapper
from random import randint

def main(stdscr):
    stdscr.nodelay(True)
    curses.init_pair(1,  curses.COLOR_BLUE, curses.COLOR_WHITE)
    rows, cols = stdscr.getmaxyx()
    rows, cols = rows - 2, cols - 2
    WHITE_AND_BLUE = curses.color_pair(1)
    stdscr.border()

    score = 0
    scoreMessage = f" Score: {score} "
    stdscr.addstr(rows+1,2, scoreMessage)

    current_x = 1
    current_y = 3
    places = [(1,1), (1,2), (1,3)]

    fruit = (randint(1, cols), randint(1, rows))
    while fruit in places: fruit = (randint(1, cols), randint(1, rows))

    stdscr.addstr(fruit[1], fruit[0], "@")

    for (x, y) in places: stdscr.addstr(y, x, "O")
    
    go = 0
    turn = "down"

    while True:

        determine = False

        key = None

        try:
            key = stdscr.getkey()
        except:
            pass

        copy_turn = turn

        if key is not None:

            determine = True

            if key == "KEY_LEFT": turn = "left"

            elif key == "KEY_RIGHT": turn = "right"

            elif key == "KEY_UP": turn = "up"

            elif key == "KEY_DOWN": turn = "down"


        buffer = (current_x, current_y)
        go = go + (score+1)/150000

        if int(go) >= 1 or determine:


            if turn == "up":
                if current_y == 1: current_y = rows
                else: current_y -= 1
            elif turn == "down":
                if current_y == rows: current_y = 1
                else: current_y += 1
            elif turn == "left":
                if current_x == 1: current_x = cols 
                else: current_x -= 1
            elif turn == "right":
                if current_x == cols: current_x = 1
                else: current_x += 1
        
            if current_x == places[-2][0] and current_y == places[-2][1]:
                current_x, current_y = buffer
                turn = copy_turn
                continue
            
            if (current_x, current_y) in places:
                message = f"YOU LOSE! TOTAL SCORE: {score}"
                stdscr.nodelay(False)
                stdscr.addstr(int((rows)/2), int((cols-len(message)+1)/2), message, curses.A_BOLD)
                stdscr.refresh()
                break



            if current_x is fruit[0] and current_y is fruit[1]: 
                score += 1
                scoreMessage = f" Score: {score} "
                stdscr.addstr(rows+1,2, scoreMessage)
                fruit = (randint(1, cols), randint(1, rows))
                while fruit in places: fruit = (randint(1, cols), randint(1, rows))
                stdscr.addstr(fruit[1], fruit[0], "@")
            else:
                stdscr.addstr(places[0][1], places[0][0], " ")
                places.pop(0)

            places.append((current_x, current_y))
            stdscr.addstr(current_y, current_x, "O")
            go = 0


        stdscr.refresh()

    stdscr.getch()

wrapper(main)